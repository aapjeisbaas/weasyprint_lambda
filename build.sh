#!/bin/sh
docker image build -t weasyprint .
docker create -ti --name dummy weasyprint bash
docker cp dummy:/opt/weasyprint.zip weasyprint_lambda_layer.zip
docker cp dummy:/opt/output.pdf ./output.pdf
docker rm dummy

